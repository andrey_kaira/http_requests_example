import 'package:flutter/material.dart';

class Person {
  final String name;
  final int age;
  final String email;
  final String image;

  const Person({
    @required this.name,
    @required this.age,
    @required this.email,
    @required this.image,
  });
}


































//factory Person.fromMap(Map<String, dynamic> map) {
//return new Person(
//name: map['name'] as String,
//age: map['age'] as String,
//email: map['email'] as String,
//image: map['image'] as String,
//);
//}
//
//Map<String, dynamic> toMap() {
//  // ignore: unnecessary_cast
//  return {
//    'name': this.name,
//    'age': this.age,
//    'email': this.email,
//    'image': this.image,
//  } as Map<String, dynamic>;
//}











//part 'person.g.dart';
//
//@JsonSerializable()
//class Person {
//
//	factory Person.fromJson(Map<String, dynamic> json) => _$PersonFromJson(json);
//	Map<String, dynamic> toJson() => _$PersonToJson(this);
//  final String name;
//  final String age;
//  final String email;
//  final String image;
//
//  const Person({
//    @required this.name,
//    @required this.age,
//    @required this.email,
//    @required this.image,
//  });
//}
