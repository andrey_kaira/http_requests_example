import 'package:http_requests_example/models/person.dart';

class PersonAdapter {
  static Person getPerson(Map<String, dynamic> map) {
    return Person(
      name: map['results'].first['name']['first'] + ' ' + map['results'].first['name']['last'],
      age: map['results'].first['dob']['age'],
      email: map['results'].first['email'],
      image: map['results'].first['picture']['large'],
    );
  }
}
